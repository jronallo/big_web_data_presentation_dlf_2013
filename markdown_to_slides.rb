#! /usr/bin/env ruby

# markdown_to_slides.rb

# Converts a markdown file into a DZslides presentation. Pandoc must be installed.
# Read in the given CSS file and insert it between script tags just before the close of the body tag.

css    = File.read('styles.css')
script = File.read('scripts.js')

`pandoc -w dzslides --standalone --self-contained presentation.md > presentation.html`

presentation = File.read('presentation.html')
style = "<style>#{css}</style>"
scripts = "<script>#{script}</script>"
presentation.sub!('</body>', "#{style}#{scripts}</body>")

File.open('presentation.html', 'w') do |fh|
  fh.puts presentation
end